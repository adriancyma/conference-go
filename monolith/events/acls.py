import requests
import json

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}

    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    params = {"q": city + " " + state, "appid": OPEN_WEATHER_API_KEY}

    geo_url = "http://api.openweathermap.org/geo/1.0/direct"

    response = requests.get(geo_url, params=params)
    content = json.loads(response.content)
    # Parse the JSON response
    # Get the latitude and longitude from the response

    latitude = content[0]["lat"]
    longitude = content[0]["lon"]

    # Create the URL for the current weather API with the latitude
    #   and longitude
    params = {
        "lat, lon": (latitude, longitude),
        "appid": OPEN_WEATHER_API_KEY,
    }

    weather_url = "https://api.openweathermap.org/data/2.5/weather"

    # Make the request
    response = requests.get(weather_url, params=params)
    content = json.loads(response.content)

    # Parse the JSON response
    try:
        # Get the main temperature and the weather's description and put
        #   them in a dictionary
        return {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except:
        return None

        # Return the dictionary
